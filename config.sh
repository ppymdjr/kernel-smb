#!/bin/bash

# to update this run the following shell command:-
# IMAGE=`imgadm avail -H -ouuid name=base-64-lts | tail -n 1`
IMAGE='ad6f47f2-c691-11ea-a6a5-cf0776f07bb7'
SOURCE_UUID="515e51b0-cd8f-11ea-88fa-6003088cb634"  # change if you fork 
ALIAS='kernel-smb'
TAG='admin'
IP='dhcp'

# for DHCP nothing else is needed
MORE_IP='' 

EXISTING=''

# I think I should build up the IP configuration more flexibly. I should use the new 'ips' property too

get_existing() {
    EXISTING=`vmadm lookup customer_metadata.source_uuid=$SOURCE_UUID`
}

get_existing_or_error() {
    get_existing
    if [[ -z $EXISTING ]]; then
        return 1;
    fi
}
