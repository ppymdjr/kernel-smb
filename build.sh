#!/bin/bash

# It would be nice to find an available IP address
source ./config.sh

imgadm import $IMAGE

# find the existing VM...
get_existing
if [[ -n $EXISTING ]]; then
    # don't continue unless we can destroy the existing
    # destroying the existing will fail if we aren't able to safely extract any data it holds
    ./destroy || exit 1 
fi

# this will create a VM using dhcp. Obviously that won't work w/o a DHCP server
echo "Creating VM"
vmadm create 2>vm-uuid <<EOF
{
  "brand":"joyent",
  "hostname":"$ALIAS",
  "image_uuid":"$IMAGE",
  "alias":"$ALIAS",
  "max_physical_memory":1024,
  "quota":0,
  "resolvers":["192.168.2.1", "8.8.8.8", "8.8.4.4"],
  "delegate_dataset": true,
  "zfs_root_compression": "on",
  "customer_metadata":{
     "source_uuid":"$SOURCE_UUID"
   },
  "nics":[
     {"nic_tag":"$TAG",
      "ip":"$IP",
      $MORE_IP
      "primary":true
   }]}

EOF

if [ "$?" -ne "0" ]; then
    cat vm-uuid
    exit 1
fi

get_existing
echo "Created VM '$EXISTING'"

cp stage2.sh /zones/$EXISTING/root/

zlogin $EXISTING sh /stage2.sh $EXISTING

GIT_DIR=.git

git config user.email robot@example.com
git config user.name $EXISTING

# If we need to commit any build artifacts we can do so here...
# git add something && git commit -m "Rebuilt zone" && git push origin master


# All done
exit 0
