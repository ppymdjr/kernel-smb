#!/bin/bash

zonename=$1

# Stage 2 installed - runs in the vm...
echo "Executing stage 2 installer in $UUID"

echo "other	password required	pam_smb_passwd.so.1	nowarn" >>/etc/pam.conf

svcadm enable smb/server
svcadm enable smb/client
svcadm enable rpc/bind
svcadm enable idmap

zfs create zones/$(zonename)/data/share1
zfs set quota=100M mountpoint=/share1 zones/$(zonename)/data/share1
chown admin:staff /share1

# the following doesn't work. How to set the password? Should I use something like vault or an LDAP server to handle auth?
# passwd admin secret

sharemgr add-share -r testSMB -s /share1 smb

svcadm enable dns/multicast
